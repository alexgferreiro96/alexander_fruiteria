import { Button, Col, Container, Row } from 'reactstrap';
import styled from 'styled-components';

const CuadradoProducto = styled.div`
    border: 2px solid red;
    padding: 15px;
    margin: 10px;
    width: 60%;
    justify-content: space-between;
`;

const CuadradoPrecio = styled.div`
    border: 2px dashed black;
    padding: 15px;
    margin: 10px;
`;
const Ticket = (props) => {

    const filas = props.cestaTicket.map((el)=>{
        return(
            <CuadradoProducto>
                <Row key={el.id + "-row-ticket"}>
                    <Col>
                        {el.nom}
                        <br />
                        {el.quantitat} X {el.preu}€/u = {(el.quantitat*el.preu).toFixed(2)}€
                    </Col>
                    <Col style={{textAlign: "center"}}>
                        <Button color={"danger"} onClick={()=>props.deleteProduct(el.id)}>Eliminar</Button>
                    </Col>
                </Row>
            </CuadradoProducto>
        )
    })

    return (
        <Container fluid>
            <Row>{filas}</Row>         
            <Row><CuadradoPrecio><Col>Preu total: {(props.totalMoney).toFixed(2)}€</Col></CuadradoPrecio></Row>
        </Container>
    )
}

export default Ticket;