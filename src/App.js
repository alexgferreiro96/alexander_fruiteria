import './App.css';
import { Col, Container, Row } from 'reactstrap';
import Productos from './Productos';
import { useState, useEffect } from 'react';
import Ticket from './Ticket';
import styled from 'styled-components';


const productes = [
  {
      "id" : 1,
      "nom" : "Plàtan",
      "preu" : 0.5
  },
  {
      "id" : 2,
      "nom" : "Poma",
      "preu": 0.8
  },
  {
      "id" : 3,
      "nom" : "Pinya",
      "preu": 5
  },
  {
      "id" : 4,
      "nom" : "Meló",
      "preu": 6
  },
];

const Titulazo = styled.h1`
  font-weight: bold;
  text-decoration: underline;
  text-align: center;
`;

function App() {

  const [cesta, setCesta] = useState([]);
  const [total,setTotal] = useState(0);

  const afegirProducte = (id) => {
    let auxArray = [...cesta];
    let aux;
    if(auxArray.find(el => el.id === id) !== undefined){
      aux = auxArray.find(el => el.id === id);
      aux.quantitat = aux.quantitat +1;
    }else{
      let producteNou = productes.find(el => el.id === id);
      producteNou.quantitat = 1;
      auxArray.push(producteNou);
    }
    setCesta(auxArray);
  }

  const borrarProducte = (id) => {
    let auxArray = [];
    cesta.forEach((el)=>{
      if(el.id !== id) auxArray.push(el);
    })
    setCesta(auxArray);
  }

  useEffect(() => {
    let money = 0;
    cesta.forEach(el=>{
      money+=(el.quantitat*el.preu);
    });
    setTotal(money);
  }, [cesta]);

  return (
    <Container fluid>
      <Titulazo>Fruteria Carmen</Titulazo>
      <Row style={{'justifyContent': 'center'}}>
        <Col>
          <Productos products={productes} addProduct={afegirProducte} />
        </Col>
        <Col>
          <Ticket cestaTicket={cesta} deleteProduct={borrarProducte} totalMoney={total}/>
        </Col>
      </Row>
    </Container>
  );
}

export default App;
