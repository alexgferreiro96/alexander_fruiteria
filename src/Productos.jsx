import { Button, Container, Row } from 'reactstrap';
import styled from 'styled-components';

const CuadradoProducto = styled.div`
    border: 2px solid green;
    padding: 15px;
    margin: 10px;
`;

const Productos = (props) => {

    const filas = [];
    props.products.forEach(el => {
        filas.push(<Row key={el.id+"-ROW"}>
            <CuadradoProducto>
                <p>{el.nom} ({el.preu}€/u) <Button color={"primary"} onClick={()=>props.addProduct(el.id)}>Afegir</Button></p>
            </CuadradoProducto>
        </Row>)
    });
    

    return (
        <Container fluid>
            {filas}
        </Container>
    )
}

export default Productos;